package br.com.desafiouol.test.apisicred.simulacoes;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	CriarSimulacoesTest.class,
	AlterarSimulacoesTest.class,
	ConsultarSimulacoesTest.class,
	RemoverSimulacoesTest.class
})
public class SimulacoesSuite {

}
