package br.com.desafiouol.test.apisicred.restricoes;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	RestricoesTest.class
})
public class RestricoesSuite {

}
