package br.com.desafiouol.test.apisicred.simulacoes;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.desafiouol.apisicred.Application;
import br.com.desafiouol.apisicred.model.ConfigurationURL;
import br.com.desafiouol.apisicred.model.ResourcesPath;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Application.class)
public class ConsultarSimulacoesTest {
	
	@Autowired
	private ConfigurationURL configurationURL;
	
	private String getSimulacoesURL(String cpf) {
		return configurationURL.getURL(ResourcesPath.SIMULACAO) +  "/" + cpf;
	}

	private String getSimulacoesURL() {
		return configurationURL.getURL(ResourcesPath.SIMULACAO);
	}
	
	@Test
//	#07
	public void consultarTodasAsSimulacoesCadastradas() {
		given()
		.when()
			.get(this.getSimulacoesURL())
		.then()
			.statusCode(200)
			.log().all()
			.body(is(not(nullValue())));
	}
	
	@Test
//	#Ultimo
	public void consultaSemSimulacoesCadastradas() {
		given()
		.when()
			.get(this.getSimulacoesURL())
		.then()
			.log().all()
			.statusCode(204);
	}
	
	@Test
//	#08
	public void consultaUmaSimulacaoCadastrada() {
		given()
		.when()
			.get(this.getSimulacoesURL("66414919004"))
		.then()
			.log().all()
			.statusCode(200)
			.body(is(not(nullValue())));
		
	}
	
	@Test
//	#09
	public void consultaUmaSimulacaoNaoCadastrada() {
		given()
		.when()
			.get(this.getSimulacoesURL("84071297042"))
		.then()
			.log().all()
			.statusCode(404)
			.body("mensagem", containsString("CPF 84071297042 não encontrado"));
	}

}
