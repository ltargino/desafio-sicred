package br.com.desafiouol.test.apisicred.restricoes;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.desafiouol.apisicred.Application;
import br.com.desafiouol.apisicred.model.ConfigurationURL;
import br.com.desafiouol.apisicred.model.ResourcesPath;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Application.class)
public class RestricoesTest {

	@Autowired
	private ConfigurationURL configurationURL;
	
	public String getRestricoesURL(String cpf) {
		return configurationURL.getURL(ResourcesPath.RESTRICAO) + "/" + cpf;
	}
	
	@Test
	public void cpfSemRestricao() {
		given()
		.when()
			.get(getRestricoesURL("07635959000"))
		.then()
			.log().all()
			.statusCode(204);
		
	}
	
	@Test
	public void cpfComRestricao() {
		given()
		.when()
			.get(getRestricoesURL("58063164083"))
		.then()
			.log().all()
			.statusCode(200)
			.body("mensagem", containsString("O CPF	58063164083 possui restrição"));
		
	}
	
	
}
