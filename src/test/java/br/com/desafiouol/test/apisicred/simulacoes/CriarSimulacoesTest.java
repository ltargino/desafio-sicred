package br.com.desafiouol.test.apisicred.simulacoes;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.desafiouol.apisicred.Application;
import br.com.desafiouol.apisicred.model.ConfigurationURL;
import br.com.desafiouol.apisicred.model.ResourcesPath;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Application.class)
public class CriarSimulacoesTest {

	@Autowired
	private ConfigurationURL configurationURL;

	private String getSimulacoesURL() {
		return configurationURL.getURL(ResourcesPath.SIMULACAO);
	}
	
	@Test
//	#01	
	public void cadastrarSimulacaoComSucesso() {
		given()
			.contentType("application/json")
			.body("{\"nome\": \"Larissa Targino\", \"cpf\": 47528346005, \"email\": \"email@email.com\", \"valor\": 1200, \"parcelas\": 3, \"seguro\": true}")
		.when()
			.post(this.getSimulacoesURL())
		.then()
			.log().all()
			.statusCode(201)
			.body(is(not(nullValue())))
			.body("nome", containsString("Larissa Targino"))
			.body("cpf", containsString("47528346005"))
			.body("email", containsString("email@email.com"))
			.body("valor", is (1200))
			.body("parcelas", is (3))
			.body("seguro", is (true));
	}
	
	@Test
//	#02	
	public void cadastrarSimulacaoComErroEmAlgumaRegra() {
		given()
			.contentType("application/json")
			.body("{\"nome\": \"Fulano de Tal\", \"cpf\": 97093236020, \"email\": \"emailemail.com\", \"valor\": 1000, \"parcelas\": 0, \"seguro\": sim}")
		.when()
			.post(this.getSimulacoesURL())
		.then()
			.log().all()
			.statusCode(400)
			.body(containsString("erros"));
	}
	
	@Test
//	#03	
	public void cadastrarSimulacaoComCpfJaExistente() {
		given()
			.contentType("application/json")
			.body("{\"nome\": \"Larissa Targino\", \"cpf\": 47528346005, \"email\": \"email@email.com\", \"valor\": 1200, \"parcelas\": 3, \"seguro\": true}")
		.when()
			.post(this.getSimulacoesURL())
		.then()
			.log().all()
			.statusCode(409)
			.body("mensagem", containsString("CPF já existente"));
	}
}

