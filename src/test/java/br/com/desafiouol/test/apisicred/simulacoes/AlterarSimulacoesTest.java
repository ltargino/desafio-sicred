package br.com.desafiouol.test.apisicred.simulacoes;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.desafiouol.apisicred.Application;
import br.com.desafiouol.apisicred.model.ConfigurationURL;
import br.com.desafiouol.apisicred.model.ResourcesPath;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Application.class)
public class AlterarSimulacoesTest {
	
	@Autowired
	private ConfigurationURL configurationURL;
	
	private String getSimulacoesURL(String cpf) {
		return configurationURL.getURL(ResourcesPath.SIMULACAO) + "/" + cpf;
	}
	
	@Test
//	#04	
	public void alterarUmaSimulacaoExistente() {
		given()
			.contentType("application/json")
			.body("{\"nome\": \"Alterando o fulano\", \"cpf\": 66414919004, \"email\": \"emaillllll@email.com\", \"valor\": 1910, \"parcelas\": 3, \"seguro\": true}")
		.when()
			.put(this.getSimulacoesURL("66414919004"))
		.then()
			.log().all()
			.statusCode(200)
			.body(is(not(nullValue())))
			.body("nome", containsString("Alterando o fulano"))
			.body("cpf", containsString("66414919008"))
			.body("email", containsString("emaillllll@email.com"))
			.body("valor", is (1910))
			.body("parcelas", is (3))
			.body("seguro", is (true));
	}
			
	@Test
//	#05
	public void alterarUmaSimulacaoExistenteComProblemaEmAlgumaRegraDeAtributo() {
		given()
			.contentType("application/json")
			.body("{\"nome\": \"Alterando o Deltrano\", \"cpf\": 17822386034, \"email\": \"emaillemail.com\", \"valor\": 500, \"parcelas\": 2, \"seguro\": true}")
		.when()
			.put(this.getSimulacoesURL("17822386034"))
		.then()
			.log().all()
			.statusCode(400)
			.body(containsString("erros"));
			
	}
	
	@Test
//	#06
	public void alterarUmaSimulacaoQueNaoExiste() {
		given()
			.contentType("application/json")
			.body("{\"nome\": \"Alterando sem simulação\", \"cpf\": 17822386034, \"email\": \"emaillemail.com\", \"valor\": 500, \"parcelas\": 2, \"seguro\": true}")
		.when()
			.put(this.getSimulacoesURL("19288254005"))
		.then()
			.log().all()
			.statusCode(404)
			.body("mensagem", containsString("CPF não encontrado"));
	}


}
