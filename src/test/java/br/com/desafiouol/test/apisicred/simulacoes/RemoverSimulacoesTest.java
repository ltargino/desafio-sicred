package br.com.desafiouol.test.apisicred.simulacoes;

import static io.restassured.RestAssured.given;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.desafiouol.apisicred.Application;
import br.com.desafiouol.apisicred.model.ConfigurationURL;
import br.com.desafiouol.apisicred.model.ResourcesPath;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Application.class)
public class RemoverSimulacoesTest {
	
	@Autowired
	private ConfigurationURL configurationURL;

	private String getSimulacoesURL(String id) {
		return configurationURL.getURL(ResourcesPath.SIMULACAO) +  "/" + id;
	}
	
	@Test
//	#10
	public void removerUmaSimulacaoCadastrada() {
		given()
		.when()
			.delete(this.getSimulacoesURL("11"))
		.then()
			.statusCode(204)
			.log().all();
	}
	
	@Test
//	#11
	public void removerSimulacaoNaoCadastrada() {
		given()
		.when()
			.delete(this.getSimulacoesURL("30"))
		.then()
			.statusCode(404)
			.log().all();
	}

}
