package br.com.desafiouol.apisicred.model;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource(value = "classpath:application.properties")
public class ConfigurationURL {

	@Value("${desafio.api.protocol}")
	private String apiProtocol;

	@Value("${desafio.api.host}")
	private String apiHost;

	@Value("${desafio.api.port}")
	private String apiPort;

	@Value("${desafio.api.applicationpath}")
	private String apiApplicationPath;

	public String getURL(ResourcesPath resourcesPath) {
		return this.apiProtocol + "://" + this.apiHost + ":" + this.apiPort + this.apiApplicationPath
				+ resourcesPath.getResourcePath();
	}

}
