package br.com.desafiouol.apisicred.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResourcesPath {

	RESTRICAO("/restricoes"), SIMULACAO("/simulacoes");

	private String resourcePath;

}
