# Desafio UOL - API Sicred

Projeto de testes automatizados para validar a API sicred, fornecida para este desafio. Este projeto faz uso de recursos como spring, maven, java 1.8, junit, rest assured, lombok, etc.

Os casos de testes estão descritos no arquivo "Caso de teste.pdf", nele está descrito os testes com seus resultados esperados e obtidos.


## Estrutura do projeto

    ├─src
    │ ├─ main
    │ │  ├─ java/br/com/desafiouol/apisicred
    │ │  │  ├─ model
    │ │  │  └─ repositories
    │ │  └─ resources
    │ │     └─ application.properties
    │ └─ test/java/br/com/desafiouol/test/apisicred
    │    ├─ restricoes
    │    └─ simulacoes
    ├─ pom.xml
    └─ README.md

## Para buildar

Na raiz do projeto, executar:

    mvn build

## Para executar os testes

Na raiz do projeto, executar:

    mvn test